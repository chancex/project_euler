# -*- coding: utf-8 -*-
'''
Problem description:
    n! means n × (n − 1) × ... × 3 × 2 × 1

    For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
    and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

    Find the sum of the digits in the number 100!

'''

import time
import math
from timer import running_time
        

@running_time
def method_1():
    pass
            



@running_time
def method_2():
    x=1
    for i in range(1,101):
        x*=i

    sum=0
    print x
    while x>0:
        sum+=x%10
        x=x/10
    print sum




if __name__ == "__main__":
    method_1()
    method_2()


