#A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91*99
#Find the largest palindrome made from the product of two 3-digit numbers.

def isPalindrome(t):
    revert=0
    i=t
    while i > 0:
        revert=revert*10 + (i % 10)
        i=i / 10
    if revert == t:
        return True
    else:
        return False


def method_1():
    number_range=999
    i=number_range
    while i >=100:
        j=number_range
        while j >=100:
            t=i*j
            if isPalindrome(t):
                print '%d * %d = %d' % (i,j,t)
                return 
            j-=1
        i-=1


def method_2():
    i=999999
    while i >= 10001:
        if isPalindrome(i):
            j=100
            while j <=999:
                if (i % j == 0) and (i / j >= 100) and (i / j <=999):
                    print i
                    print '%d * %d = %d' % (j,i/j,i)
                    return
                j+=1
        i-=1

if __name__ == "__main__":
    method_1()
    method_2()
