# -*- coding: utf-8 -*-
'''
Problem description:
    The following iterative sequence is defined for the set of positive integers:

        n → n/2 (n is even)
        n → 3n + 1 (n is odd)

        Using the rule above and starting with 13, we generate the following sequence:

        13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
        It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

        Which starting number, under one million, produces the longest chain?

        NOTE: Once the chain starts the terms are allowed to go above one million.

'''

import time
import math
from timer import running_time
        
sequence=1000000
#record the terms of every n
index_test={}

def terms_of_n(n):
    global index_test

    if n==1:
        return 1
    elif str(n) in index_test:
        return index_test[str(n)]
    elif n % 2 == 0:
        index_test[str(n)] = 1+terms_of_n(n/2)
        return index_test[str(n)]
    else:
        index_test[str(n)] = 1+terms_of_n(3*n+1)
        return index_test[str(n)]

#rescursive solution
@running_time
def method_1():
    global index_test

    max_term=0
    start_n=0
    for i in range(1,sequence+1):
        temp=terms_of_n(i)
        if temp > max_term:
            max_term=temp
            start_n=i

    print 'starting number is:%d, term is:%d' % (start_n, max_term)

            


# loop solution
@running_time
def method_2():
    global sequence

    temp_dict={'1':1}
    start_n=0
    max_term=0
    for i in range (2,sequence+1):
        if str(i) in temp_dict:
            continue
        term=1
        k=i
        while k>1:
            if k % 2 ==0:
                k=k/2
            else:
                k=3*k+1
            if str(k) in temp_dict:
                term+=temp_dict[str(k)]
                break
            term+=1
        temp_dict[str(i)]=term
        if temp_dict[str(i)]>max_term:
            max_term=temp_dict[str(i)]
            start_n=i

    print 'starting number is:%d, term is:%d' % (start_n, max_term)


term_array=(sequence+1)*[0]

def terms_of_n_with_array(n):
    global term_array
    global sequence
    if n==1:
        return 1
    elif n<=sequence and term_array[n]!=0:
        return term_array[n]
    elif n % 2 == 0:
        temp = 1+terms_of_n_with_array(n/2)
    else:
        temp = 1+terms_of_n_with_array(3*n+1)
    if n <=sequence:
        term_array[n]=temp
    return temp


@running_time
def method_3():
    global index_test

    max_term=0
    start_n=0
    for i in range(1,sequence+1):
        temp=terms_of_n_with_array(i)
        if temp > max_term:
            max_term=temp
            start_n=i

    print 'starting number is:%d, term is:%d' % (start_n, max_term)


if __name__ == "__main__":
    method_1()
    method_2()
    method_3()


