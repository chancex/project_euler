# -*- coding: utf-8 -*-
'''
Problem description:
    Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

    How many such routes are there through a 20×20 grid?

    https://projecteuler.net/problem=15

'''

import time
import math
from timer import running_time
        

GRID_SPAN=20
def routes_to_bottom_right(x,y):
    global GRID_SPAN
    if x == GRID_SPAN:
        if y == GRID_SPAN:
            return 1
        else:
            return routes_to_bottom_right(x,y+1)
    elif y == GRID_SPAN:
        return routes_to_bottom_right(x+1,y)
    else:
        return routes_to_bottom_right(x+1,y)+routes_to_bottom_right(x,y+1)


@running_time
def method_1():
    print routes_to_bottom_right(0,0)
            



@running_time
def method_2():
    grid=[[0 for i in range(GRID_SPAN+1)] for j in range(GRID_SPAN+1)]
    for i in range(0,GRID_SPAN+1): 
        grid[GRID_SPAN][i]=1
        grid[i][GRID_SPAN]=1

    for i in range(GRID_SPAN-1,-1,-1):
        for j in range(i,-1,-1):
            grid[i][j]=grid[i+1][j]+grid[i][j+1]
            grid[j][i]=grid[j+1][i]+grid[j][i+1]

    print grid[0][0]


if __name__ == "__main__":
#    method_1()
    method_2()


