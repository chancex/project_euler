#
from time import time
import math

    
def method_1():
    count=0
    i=1
    while count<10001:
        i+=1
        j=2
        while j <= int(math.sqrt(i)):
            if i % j == 0:
                break
            j+=1
        if j == int(math.sqrt(i))+1:
            count+=1
    print i



def method_2():
    pass
    


if __name__ == "__main__":
    start_time=time()
    print "start at %f" % start_time
    method_1()
    end_time=time()
    print "end at %f" % end_time
    print "total computing time: %f seconds" % (end_time-start_time)
    start_time=time()
    print "method 2 start at %f" % start_time
    method_2()
    print "method 2 end at %f" % end_time
    print "method2 total computing time: %f seconds" % (end_time-start_time)
