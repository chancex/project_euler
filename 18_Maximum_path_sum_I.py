# -*- coding: utf-8 -*-
'''
Problem description:
    By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

    3
    7 4
    2 4 6
    8 5 9 3

    That is, 3 + 7 + 4 + 9 = 23.
    Find the maximum total from top to bottom of the triangle below:

'''

import time
import math
from timer import running_time
        

path_grid=[
    [75],
    [95, 64],
    [17, 47, 82],
    [18, 35, 87, 10],
    [20,  4, 82, 47, 65],
    [19,  1, 23, 75,  3, 34],
    [88,  2, 77, 73,  7, 63, 67],
    [99, 65,  4, 28,  6, 16, 70, 92],
    [41, 41, 26, 56, 83, 40, 80, 70, 33],
    [41, 48, 72, 33, 47, 32, 37, 16, 94, 29],
    [53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14],
    [70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57],
    [91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48],
    [63, 66, 4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31],
    [ 4, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60,  4, 23]
]


@running_time
def method_1():
    sum_grid=[]
    for i in range (0,len(path_grid)-1):
        temp=[0]*len(path_grid[i])
        sum_grid.append(temp)
    sum_grid.append(path_grid[len(path_grid)-1])



    for i in range (0,len(sum_grid)):
        print sum_grid[i]

    print sum_grid[3][3]

    for i in range(len(path_grid)-1,0,-1):
        for j in range(0,len(path_grid[i])-1):
            sum_grid[i-1][(2*j+1)/2]= path_grid[i-1][(2*j+1)/2]+max(sum_grid[i][j],sum_grid[i][j+1])
        print sum_grid[i-1]
            


@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


