# -*- coding: utf-8 -*-
'''
Problem description:
    If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

    If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?


    NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

'''

import time
import math
from timer import running_time


number_to_letter={
    1:'one',
    2:'two',
    3:'three',
    4:'four',
    5:'five',
    6:'six',
    7:'seven',
    8:'eight',
    9:'nine',
    10:'ten',
    11:'eleven',
    12:'twelve',
    13:'thirteen',
    14:'fourteen',
    15:'fifteen',
    16:'sixteen',
    17:'seventeen',
    18:'eighteen',
    19:'nineteen',
    20:'twenty',
    30:'thirty',
    40:'forty',
    50:'fifty',
    60:'sixty',
    70:'seventy',
    80:'eighty',
    90:'ninety',
    100:'hundred',
    1000:'thousand'
}



def count_letters(n):
    i=n
    count=0
    letter=''
    need_to_add_and=False

    if n >100:
        need_to_add_and=True

    is_and_set=False
    
    while i>0:
        if i >= 1000:
            thousand_bit=i / 1000
            count+=len(number_to_letter[thousand_bit])+len(number_to_letter[1000])
            letter=letter+number_to_letter[thousand_bit]+' '+number_to_letter[1000]
            i=i%1000
        elif i >=100:
            hundred_bit=(i / 100)
            count+=len(number_to_letter[hundred_bit])+len(number_to_letter[100])
            letter=letter+' '+number_to_letter[hundred_bit]+' '+number_to_letter[100]
            i=i%100

        elif i >=20:
            if need_to_add_and == True and is_and_set==False:
                letter=letter+' '+'and'
                count+=len('and')
                is_and_set=True

            ten_bit=(i / 10) * 10
            count+=len(number_to_letter[ten_bit])
            letter=letter+' '+number_to_letter[ten_bit]
            i=i%10

        else:
            if need_to_add_and == True and is_and_set==False:
                letter=letter+' '+'and'
                count+=len('and')

            count+=len(number_to_letter[i])
            letter=letter+' '+number_to_letter[i]
            break


    print 'n=%d, letter=%s, count=%d' % (n, letter,count)
    return count





            



@running_time
def method_1():
    sum=0
    for i in range(1,1001):
        sum+=count_letters(i)

    print 'total letters are:%d' % sum

            



@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


