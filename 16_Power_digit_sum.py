# -*- coding: utf-8 -*-
'''
Problem description:

'''

import time
import math
from timer import running_time
        
@running_time
def method_1():

    big_int=[1]
    for j in range(1,1001):
        overflow=0
        for i in range(0,len(big_int)):
            temp=big_int[i]*2+overflow
            if temp >= 10:
                if i == len(big_int)-1:
                    big_int.append(temp/10)
                    big_int[i]=temp % 10
                    overflow=0
                else:
                    overflow=temp/10
                    big_int[i]=temp % 10
            else:
                big_int[i]=temp
                overflow=0

    sum=0
    for i in range(0,len(big_int)):
        sum+=big_int[i]

    print sum



            



@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


