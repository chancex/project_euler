# -*- coding: utf-8 -*-
'''
Problem description:

Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
'''

import time
import math
from timer import running_time

def sum_of_proper_divisors(n):
    sum=0
    for i in range(1,n):
        if n % i == 0:
            sum += i
    return sum


@running_time
def method_1():
    sum_of_amicable_numbers = 0
    for i in range (2, 10000):
        temp = sum_of_proper_divisors(i)
        if (temp != i) and (sum_of_proper_divisors(temp) == i):
            sum_of_amicable_numbers +=i;
            print "i=%d, j=%d" % (i, temp)
    print sum_of_amicable_numbers
            



@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


