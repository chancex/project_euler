# -*- coding: utf-8 -*-
#The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
#Find the sum of all the primes below two million.

from time import time
import math




def isPrime(number):
    i=2
    while i <= math.sqrt(number):
        if number % i == 0:
            return False
        i+=1
    return True
    
def method_1():
    sum=0
    for i in range(2,2000000):
        if isPrime(i):
            sum+=i
    print sum


def method_2():
    sum=2
    for i in range(3,2000000,2):
        j=3
        while j <= math.sqrt(i):
            if i % j == 0:
                print j+' ',
                break 
            j+=2
        if j<=math.sqrt(i):
            continue
        sum+=i
    print sum
    
#使用埃拉托斯特尼筛法
def method_3():
    number=2000000
    number_list=[True]*(number+2)

    i=2
    while i <=math.sqrt(number):
        j=2
        index=j*2
        while index<number:
            number_list[index]=False
            j+=1
            index=i*j 
        i+=1
        while i<=math.sqrt(number) and number_list[i]==False: 
            i+=1

    sum=0
    for count in range(2,number):
        if number_list[count]==True:
            sum+=count

    print sum




def timer(tag):
    current=time()
    print "%s at %f" % (tag,current)
    return current

if __name__ == "__main__":
    start=timer('start')
    #method_1()
    end=timer('end')
    print "total time: %f seconds" % (end-start)

    start=timer('start')
    #method_2()
    end=timer('end')
    print "total time: %f seconds" % (end-start)

    start=timer('start')
    method_3()
    end=timer('end')
    print "total time: %f seconds" % (end-start)
