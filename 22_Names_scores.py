# -*- coding: utf-8 -*-
'''
Problem description:

    Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

    For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.

    What is the total of all the name scores in the file?

'''

import time
import math
from timer import running_time


def get_score(name):
    print name
    score = 0
    for c in name:
        score += ord(c) - ord('A') +1
    return score

@running_time
def method_1():
    sum_of_score = 0
    with open('22_names.txt','r') as f:
        lines = f.read()
        names=[x.strip('"') for x in lines.split(',')]

        names.sort()

        for i, name in enumerate(names):
            print "%d, %s" % (i+1, name)
            sum_of_score += (i+1)*get_score(name)

    print sum_of_score

            



@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


