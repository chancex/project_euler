#The prime factors of 13195 are 5, 7, 13 and 29.

#What is the largest prime factor of the number 600851475143 ?
from time import time

    
def method_1():
    i=20
    #for i in xrange(2520,5040000):
    while True:
        j=2
        while j <= 20:
            if i % j != 0:
                break
            j+=1
        if j==21:
            print i
            return
        i+=20



def gcd(a, b):
    """Return greatest common divisor using Euclid's Algorithm."""
    while b:      
        a, b = b, a % b
    return a

def lcm(a, b):
    """Return lowest common multiple."""
    return a * b // gcd(a, b)

def lcmm(*args):
    """Return lcm of args."""   
    return reduce(lcm, args)


def method_2():
    print lcmm(*range(1,20))
    


if __name__ == "__main__":
    start_time=time()
    print "start at %f" % start_time
    method_1()
    end_time=time()
    print "end at %f" % end_time
    print "total computing time: %f seconds" % (end_time-start_time)
    start_time=time()
    print "method 2 start at %f" % start_time
    method_2()
    print "method 2 end at %f" % end_time
    print "method2 total computing time: %f seconds" % (end_time-start_time)
