# -*- coding: utf-8 -*-
'''
Problem description:
    You are given the following information, but you may prefer to do some research for yourself.

    1 Jan 1900 was a Monday.
    Thirty days has September,
    April, June and November.
    All the rest have thirty-one,
    Saving February alone,
    Which has twenty-eight, rain or shine.
    And on leap years, twenty-nine.
    A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
    How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?

'''

import time
import math
from timer import running_time

average_year_months=[0,31,28,31,30,31,30,31,31,30,31,30,31]
leap_year_months=[0,31,29,31,30,31,30,31,31,30,31,30,31]
        

def is_leap_year(year):
    assert year >= 0
    if (year % 400 == 0) or (year % 4 ==0 and year % 100 != 0):
        return True
    else:
        return False


@running_time
def method_1():
    assert is_leap_year(1900)==False
    assert is_leap_year(1902)==False
    assert is_leap_year(1903)==False
    assert is_leap_year(1904)==True

    day=1 #initial day. monday on 1900-1-1. 
    total_sunday_counts=0

    for i in range(1900,2001):
        if is_leap_year(i):
            year_months=leap_year_months
        else:
            year_months=average_year_months

        for j in range(1,13):
            for k in range(1,year_months[j]+1):
                if i>1900 and k==1 and day == 7:
                    total_sunday_counts+=1
                    print 'shot:%d-%d-%d' % (i,j,k)
                day+=1
                if day>7:
                    day=1

    print 'sunday fell in first of the month is:%d' % total_sunday_counts

@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


