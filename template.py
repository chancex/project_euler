# -*- coding: utf-8 -*-
'''
Problem description:

'''

import time
import math
from timer import running_time
        

@running_time
def method_1():
    pass
            



@running_time
def method_2():
    pass



if __name__ == "__main__":
    method_1()
    method_2()


