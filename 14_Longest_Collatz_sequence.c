#include <stdio.h>
#include <string.h>
#include <time.h>



#define SEQUENCE 1000000

long cache[SEQUENCE+1]; 


long terms_of_n(long i){
    long temp;
    if(i==1){
        return 1;
    }
    else if(i<=SEQUENCE && cache[i]!=0){
        return cache[i];
    }
    else if(i % 2 ==0){
        temp=1+terms_of_n(i/2);
    }
    else{
        temp=1+terms_of_n(i*3+1);
    }
    if(i<=SEQUENCE){
        cache[i]=temp;
    }
    return temp;
}




void main(){
    memset(cache,0,sizeof(long));
    long start_n=0;
    long max_term=0;
    long i;

    clock_t t;
    t = clock();

    for (i=1;i<=SEQUENCE;i++){
        long temp=terms_of_n(i);
        if(temp>max_term){
            max_term=temp;
            start_n=i;
        }
    }

    printf("the start number is:%d,and the term is:%d",start_n,max_term);
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; 
    printf("fun() took %f seconds to execute \n", time_taken);
}

        


