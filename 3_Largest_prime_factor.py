#The prime factors of 13195 are 5, 7, 13 and 29.

#What is the largest prime factor of the number 600851475143 ?
from time import time
import math

def isPrime(number):
    i=2
    while i <= math.sqrt(number):
        if number % i == 0:
            return False
        i+=1
    return True
    
def method_1():
    dividend=600851475143
    i= dividend / 2
    j=2

    while i >= 1:
        if dividend % i == 0 and isPrime(i):
            print "the largest prime factor is:%d" % i
            return
        j+=1
        i=dividend/j

            

def method_2():
    x = 600851475143
    lpf = 3

    while x > 1:
        f = lpf
        if x % f == 0:
            x //= f
        else:
            lpf += 2

    print lpf


if __name__ == "__main__":
    start_time=time()
    print "start at %f" % start_time
#    method_1()
    end_time=time()
    print "end at %f" % end_time
    print "total computing time: %f seconds" % (end_time-start_time)
    method_2()
