# -*- coding: utf-8 -*-
#A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

#a2 + b2 = c2
#For example, 32 + 42 = 9 + 16 = 25 = 52.

#There exists exactly one Pythagorean triplet for which a + b + c = 1000.
#Find the product abc.

from time import time
import math


    
def method_1():

# start
    for a in range(2,333):
        for b in range(a+1,499):
            if a+2*b >=1000:
                break
            for c in range(b+1,1000-a-b+1):
                if a+b+c==1000:
                    break

            if a*a + b*b == c*c:
                    print "%d*%d*%d=%d" % (a, b, c, a*b*c)
                    return




def method_2():
    for a in range(2,333):
        for b in range(a+1,499):
            c=math.sqrt(a*a+b*b)
            if c>b and a+b+c ==1000:
                print a*b*c
    


def timer(tag):
    current=time()
    print "%s at %f" % (tag,current)
    return current

if __name__ == "__main__":
    start=timer('start')
    method_1()
    end=timer('end')
    print "total time: %f seconds" % (end-start)

    start=timer('start')
    method_2()
    end=timer('end')
    print "total time: %f seconds" % (end-start)
